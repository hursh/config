# Reload as needed
alias reload='source ~/.bash_profile'

# Path export
export PATH=$PATH:/usr/bin/:/usr/local/git/bin/:/usr/local/share/npm/bin

# Git related commands
# if not a git plugin
alias commits='git log --branches --not --remotes --decorate'
alias l='ls -G'
alias ll='ls -alhG'
alias lol='git log --date-order --all --graph --format="%C(green)%h%Creset %C(yellow)%an%Creset %C(blue bold)%ar%Creset %C(red bold)%d%Creset%s"'
alias st='git status'

# Mac specific
alias spotlightIndexOn='sudo mdutil -a -i on'
alias spotlightIndexOff='sudo mdutil -a -i off'

# Make jrnl note taking easier
alias j=jrnl

# 'nu' number up folders
alias 1u='cd ..'
alias 2u='cd ../..'
alias 3u='cd ../../..'
alias 4u='cd ../../../..'

# simple ip
alias ip='ifconfig | grep "inet " | grep -v 127.0.0.1 | cut -d\ -f2'
# more details
alias ip1="ifconfig -a | perl -nle'/(\d+\.\d+\.\d+\.\d+)/ && print $1'"
# external ip
alias ip2="curl -s http://www.showmyip.com/simple/ | awk '{print $1}'"

alias reload='source ~/.bash_profile'

export NVM_DIR="/Users/hurshprasad/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

nvm install 0.10.33

export PS1="👤 \[\e[4;32m\]Ĥµ®§ĥ\[\e[0m\] \[\e[0;34m\]\W\[\e[0m\] "


[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

alias couchdb_lucene='/usr/local/Cellar/couchdb-lucene/1.0.2/libexec/bin/run'

alias dockerDeleteAllImages='docker images -q | xargs -n 1 docker rmi --force=true'
alias dockerDeleteAllContainers='docker ps -aq | xargs -n 1 docker rm --force=true'
alias dockerStopAllContainers='docker ps -aq | xargs -n 1 docker stop'
