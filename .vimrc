" ******************************************
" - .vimrc
" - Vundle is the exclusive package manager
" - all plugins works with Bundle convention
" - all config is in vimrc so it can be kept
"   nice and clean with out missing config
" - all plugin config is at the bottom
" ******************************************


" Vundle template settings
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" defaults
Bundle 'kien/ctrlp.vim'
Bundle 'flazz/vim-colorschemes'
Bundle 'bling/vim-airline'
Bundle 'tpope/vim-git'
Bundle 'tpope/vim-fugitive'
Bundle 'tmhedberg/matchit'
Bundle 'tpope/vim-surround'
Bundle 'tristen/vim-sparkup'
Bundle 'tpope/vim-dispatch'
Bundle 'editorconfig/editorconfig-vim'
Bundle 'SirVer/ultisnips'
Bundle 'honza/vim-snippets'

" frontend development
Bundle 'scrooloose/syntastic'
Bundle 'pangloss/vim-javascript'
Bundle 'majutsushi/tagbar'
Bundle 'othree/javascript-libraries-syntax.vim'
Bundle 'burnettk/vim-angular'
Bundle 'drmingdrmer/xptemplate'

" Note Taking
Bundle 'xolox/vim-misc'
Bundle 'xolox/vim-notes'

" experimenting
Bundle 'airblade/vim-gitgutter'
Bundle 'elzr/vim-json'
Bundle 'michalliu/jsruntime.vim'
Bundle 'michalliu/jsoncodecs.vim'
Bundle 'maksimr/vim-jsbeautify'
Bundle 'einars/js-beautify'
Bundle 'mklabs/grunt.vim'
Bundle 'ingydotnet/yaml-vim'
Bundle 'jeetsukumaran/vim-buffergator'
"Bundle 'michalliu/sourcebeautify.vim'
Bundle 'ryanss/vim-hackernews'
" may not need
Bundle 'scrooloose/nerdtree'
Bundle 'jistr/vim-nerdtree-tabs'

" had conflict
"Bundle 'msanders/snipmate.vim'


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
" *********************************
" CtrlP 
" *********************************
map <C-B> :CtrlPBuffer<CR>

"Use TAB to complete when typing words, else inserts TABs as usual.
""Uses dictionary and source files to find matching words to complete.

"See help completion for source,
""Note: usual completion is on <C-n> but more trouble to press all the time.
"Never type the same word twice and maybe learn a new spellings!
""Use the Linux dictionary when spelling is in doubt.
"Window users can copy the file to their machine.

function! Tab_Or_Complete()
	if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
	      return "\<C-N>"
	else
	    return "\<Tab>"
	endif
endfunction

inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>
set dictionary="/usr/dict/words"

" *********************************
" visual config
" *********************************

syntax on
set background=dark
"colorscheme jellybeans
"colorscheme coffee
colorscheme molokai
set list
nmap <leader>l :set list!<CR>
set listchars=tab:▸\ ,eol:¬,trail:.,precedes:<,extends:>,nbsp:_

" *********************************
" Note Rules
" *********************************
let g:notes_directories = ['~/Documents/UCL/Notes']
let g:note_list_bullets = ['•', '◦', '▸', '▹', '▪', '▫']
let g:notes_suffix = '.md'

" *********************************
" NerdTree Config
" *********************************

let NERDTreeQuitOnOpen=1
let NERDTreeShowHidden=1

silent! nmap <C-z> :NERDTreeToggle<CR>
silent! map <F1> :NERDTreeFind<CR>

let g:NERDTreeMapActivateNode="<F1>"
let g:NERDTreeMapPreview="<F2>"

" Close buffers in the correct way
function! s:CloseIfOnlyControlWinLeft()
  if winnr("$") != 1
    return
  endif
  if (exists("t:NERDTreeBufName") && bufwinnr(t:NERDTreeBufName) != -1)
        \ || &buftype == 'quickfix'
    q
  endif
endfunction
augroup CloseIfOnlyControlWinLeft
  au!
  au BufEnter * call s:CloseIfOnlyControlWinLeft()
augroup END

" *********************************
" airline configurations
" *********************************

set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts=0
let g:airline_theme='badwolf'
let g:airline_left_sep=''
let g:airline_right_sep=''


if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif

 "unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'
au BufRead,BufNewFile *.json setf json

" *********************************
" JS/HTML/CSS Beautify Rules
" *********************************

map <c-f> :call JsBeautify()<cr>
" or
autocmd FileType javascript noremap <buffer>  <c-f> :call JsBeautify()<cr>
" for html
autocmd FileType html noremap <buffer> <c-f> :call HtmlBeautify()<cr>
" for css or scss
autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>

" *********************************
" File Type Rules
" *********************************

au BufNewFile,BufRead *.raml filetype=yaml

set foldmethod=syntax
set foldlevelstart=1

let javaScript_fold=1         " JavaScript
let perl_fold=1               " Perl
let php_folding=1             " PHP
let r_syntax_folding=1        " R
let ruby_fold=1               " Ruby
let sh_fold_enabled=1         " sh
let vimsyn_folding='af'       " Vim script
let xml_syntax_folding=1      " XML

" *********************************
" Basic Rules
" *********************************

let g:bnext="<F3>"
" more inutive split
set relativenumber
set number
"set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
set noswapfile
set nofoldenable
set nowrap
set backspace=indent,eol,start
set timeout timeoutlen=1000 ttimeoutlen=100
set autoread
set clipboard+=unnamed
" Ignore case when searching
set ignorecase
"
" " When searching try to be smart about cases 
set smartcase
"
" " Highlight search results
set hlsearch
"
" " Makes search act like search in modern browsers
set incsearch
"
" " Don't redraw while executing macros (good performance config)
set lazyredraw
"
" " For regular expressions turn magic on
set magic
"
" " Show matching brackets when text indicator is over them
set showmatch
" " How many tenths of a second to blink when matching brackets
set mat=2
"
" " No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500
" " save as sudo inside vim
cmap w!! w !sudo tee > /dev/null %
